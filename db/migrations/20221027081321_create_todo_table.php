<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateTodoTable extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('todos');
        $table->addColumn('name', 'string')
            ->addColumn('content', 'text')
            ->addColumn('created', 'datetime')
            ->create();
    }
}
