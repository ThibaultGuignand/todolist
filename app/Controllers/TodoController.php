<?php

namespace App\Controllers;

use App\Models\Todo;

class TodoController extends Controller
{
    public function index(): string
    {

        $todoModel = new Todo();
        $todos = $todoModel->all();

        [$checked_todos, $in_progress_todos] = $this->getTodosByChecked($todos);

        return $this->template->render('home.html', [
            'checked_todos' => $checked_todos,
            'in_progress_todos' => $in_progress_todos,
        ]);
    }

    public function create(): string
    {

        return $this->template->render('createTask.html', [
        ]);
    }

    public function show(int $id): string
    {
        $todoModel = new Todo();
        $todo = $todoModel->get($id);

        return $this->template->render('todos/show.html', [
            'todo' => $todo
        ]);
    }

    private function getTodosByChecked(array $todos): array
    {
        $checked_todos = [];
        $in_progress_todos = [];
        foreach ($todos as $todo) {
            if ($todo->is_checked) {
                $checked_todos[] = $todo;
            } else {
                $in_progress_todos[] = $todo;
            }
        }

        return [$checked_todos, $in_progress_todos];
    }

    public function checked(int $id): string
    {
        $todoModel = new Todo();
        $todoModel->checked($id);

        return $this->template->render('todos/checked.html');
    }

    public function createArticle(string $title, string $description)
    {
        if (isset($_POST['title']) && !empty($_POST['title'])
            && isset($_POST['description']) && !empty($_POST['description'])) {
            $title = $_POST['title'];
            $description = $_POST['description'];
        }else{
            echo "Dommage il y a une erreur";
        }


        $todoModel = new Todo();
        $todoModel ->create($title, $description);

        header('location: /');
        exit();
    }

    public function __isset(string $name): bool
    {
        // TODO: Implement __isset() method.
    }

}
