<?php

namespace App\Models;

use IdiormResultSet;
use ORM;

class Todo extends Model
{
    static protected string $table = 'todos';

    public function all(): IdiormResultSet|array
    {
        return ORM::forTable(self::$table)->findMany();
    }

    public function get(int $id): ORM|bool
    {
        return ORM::forTable(self::$table)->findOne($id);
    }

    public function checked(int $id): bool
    {
        $todo = $this->get($id);
        $todo->is_checked = true;
        $todo->save();

        return true;
    }

    public function create(string $title, string $content): void
    {
        $todo = ORM::forTable('todos')->create();
        $todo->name=$title;
        $todo->content=$content;
        $todo->save();
    }
}
