<?php

use App\Controllers\TodoController;

require_once './vendor/autoload.php';

$todo = new TodoController();
switch (getUri()) {
    case '/':
        echo $todo->index();
        break;
    case '/todo':
        if(!isset($_GET['id'])) {
            header('Location: /');
            exit;
        }

        echo $todo->show($_GET['id']);
        break;
    case '/checked':
        if(!isset($_GET['id'])) {
            header('Location: /');
            exit;
        }

        echo $todo->checked($_GET['id']);
        break;

    case '/createTask':
        echo $todo->createArticle($_POST ['title'],$_POST ['description']);
        break;

    case '/createForm':
        echo $todo->create();
        break;

}
